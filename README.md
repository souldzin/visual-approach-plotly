# visual-approach-plotly

-   https://dash.plotly.com/
-   https://nivo.rocks/

## Get started

1. Install environment

    ```shell
    asdf install
    ```

2. Set up python virtual environment

    ```shell
    # First time
    python -m venv venv

    # Every time
    source venv/bin/activate
    ```

3. Install npm packages

    ```
    $ npm install
    ```

4. Install python packages required to build components.
    ```
    $ pip install -r requirements.txt
    ```
5. Install the python packages for testing (optional)
    ```
    $ pip install -r tests/requirements.txt
    ```

## How to run (according to Plotly)?

1. Install Dash and its dependencies: https://dash.plotly.com/installation
2. Run `python usage.py`
3. Visit http://localhost:8050 in your web browser

## Tools we are using

-   [asdf](https://asdf-vm.com/) - this is used to manage the development environment.
    When installing, make sure you update the `zsh_profile` or `bash_profile` which
    `brew` will tell you to do anyways.

## More docs

-   [Docs from Plotly](./docs/dev/plotly.md)
