% Auto-generated: do not edit by hand
\name{nivoTest}

\alias{nivoTest}

\title{NivoTest component}

\description{
ExampleComponent is an example component. It takes a property, `label`, and displays it. It renders an input with the property `value` which is editable by the user.
}

\usage{
nivoTest(id=NULL)
}

\arguments{
\item{id}{Character. The ID used to identify this component in Dash callbacks.}
}

\value{named list of JSON elements corresponding to React.js properties and their values}

