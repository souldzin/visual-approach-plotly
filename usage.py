import visual_approach_plotly
from dash import Dash, callback, html, Input, Output

app = Dash(__name__)

app.layout = html.Div([
    visual_approach_plotly.HelloWorld(
        id='input',
        value='my-value',
        label='my-label'
    ),
    html.Div(id='output'),
    visual_approach_plotly.NivoTest(id='nivo_id')
])


@callback(Output('output', 'children'), Input('input', 'value'))
def display_output(value):
    return 'You have entered {}'.format(value)


if __name__ == '__main__':
    app.run_server(debug=True)
