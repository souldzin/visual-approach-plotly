# AUTO GENERATED FILE - DO NOT EDIT

export nivotest

"""
    nivotest(;kwargs...)

A NivoTest component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.
Keyword arguments:
- `id` (String; optional): The ID used to identify this component in Dash callbacks.
"""
function nivotest(; kwargs...)
        available_props = Symbol[:id]
        wild_props = Symbol[]
        return Component("nivotest", "NivoTest", "visual_approach_plotly", available_props, wild_props; kwargs...)
end

