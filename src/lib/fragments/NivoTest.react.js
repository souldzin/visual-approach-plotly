import React, {Component} from 'react';
import {defaultProps, propTypes} from '../components/NivoTest.react';

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class NivoTest extends Component {
    render() {
        const {id} = this.props;

        return <div id={id}>Nivo is coming!</div>;
    }
}

NivoTest.defaultProps = defaultProps;
NivoTest.propTypes = propTypes;
