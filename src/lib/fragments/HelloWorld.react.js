import React, {Component} from 'react';
import {defaultProps, propTypes} from '../components/HelloWorld.react';

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class HelloWorld extends Component {
    render() {
        const {id, label, setProps, value} = this.props;

        return (
            <div id={id}>
                Hello world: {label}&nbsp;
                <input
                    value={value}
                    onChange={
                        /*
                         * Send the new value to the parent component.
                         * setProps is a prop that is automatically supplied
                         * by dash's front-end ("dash-renderer").
                         * In a Dash app, this will update the component's
                         * props and send the data back to the Python Dash
                         * app server if a callback uses the modified prop as
                         * Input or State.
                         */
                        (e) => setProps({value: e.target.value})
                    }
                />
            </div>
        );
    }
}

HelloWorld.defaultProps = defaultProps;
HelloWorld.propTypes = propTypes;
