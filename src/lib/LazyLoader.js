import React from 'react';

export const HelloWorld = React.lazy(() =>
    import(/* webpackChunkName: "HelloWorld" */ './fragments/HelloWorld.react')
);

export const NivoTest = React.lazy(() =>
    import(/* webpackChunkName: "NivoTest" */ './fragments/NivoTest.react')
);
