/* eslint no-magic-numbers: 0 */
import React, {Component} from 'react';

import {HelloWorld, NivoTest} from '../lib';

class App extends Component {
    constructor() {
        super();
        this.state = {
            value: '',
            helloWorldLabel: 'Greetings from [App.js]',
        };
    }

    render() {
        return (
            <div>
                <h1>Webpack Server (FRONTEND ONLY!)</h1>
                <div className="border border-solid border-slate-500 mt-3 py-6 px-3">
                    <p>components: HelloWorld</p>
                    <HelloWorld
                        setProps={({value}) => this.setState({value})}
                        value={this.state.value}
                        label={this.state.helloWorldLabel}
                    />
                </div>
                <div className="border border-solid border-slate-500 mt-3 py-6 px-3">
                    <p>components: NivoTest</p>
                    <NivoTest />
                </div>
                <div className="border border-solid border-slate-500 mt-3 py-6 px-3">
                    <p>Demo App State</p>
                    <pre>{JSON.stringify(this.state, null, 2)}</pre>
                </div>
            </div>
        );
    }
}

export default App;
