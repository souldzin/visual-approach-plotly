
module VisualApproachPlotly
using Dash

const resources_path = realpath(joinpath( @__DIR__, "..", "deps"))
const version = "0.0.1"

include("jl/helloworld.jl")
include("jl/nivotest.jl")

function __init__()
    DashBase.register_package(
        DashBase.ResourcePkg(
            "visual_approach_plotly",
            resources_path,
            version = version,
            [
                DashBase.Resource(
    relative_package_path = "async-HelloWorld.js",
    external_url = "https://unpkg.com/visual_approach_plotly@0.0.1/visual_approach_plotly/async-HelloWorld.js",
    dynamic = nothing,
    async = :true,
    type = :js
),
DashBase.Resource(
    relative_package_path = "async-NivoTest.js",
    external_url = "https://unpkg.com/visual_approach_plotly@0.0.1/visual_approach_plotly/async-NivoTest.js",
    dynamic = nothing,
    async = :true,
    type = :js
),
DashBase.Resource(
    relative_package_path = "async-HelloWorld.js.map",
    external_url = "https://unpkg.com/visual_approach_plotly@0.0.1/visual_approach_plotly/async-HelloWorld.js.map",
    dynamic = true,
    async = nothing,
    type = :js
),
DashBase.Resource(
    relative_package_path = "async-NivoTest.js.map",
    external_url = "https://unpkg.com/visual_approach_plotly@0.0.1/visual_approach_plotly/async-NivoTest.js.map",
    dynamic = true,
    async = nothing,
    type = :js
),
DashBase.Resource(
    relative_package_path = "visual_approach_plotly.min.js",
    external_url = nothing,
    dynamic = nothing,
    async = nothing,
    type = :js
),
DashBase.Resource(
    relative_package_path = "visual_approach_plotly.min.js.map",
    external_url = nothing,
    dynamic = true,
    async = nothing,
    type = :js
)
            ]
        )

    )
end
end
