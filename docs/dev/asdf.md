# asdf Examples

## Install this project's environment

```shell
asdf install
```

## Adding a new plugin

Say, asdf has never seen python before and responds with a message

```shell
No such plugin: python
```

Just run:

```shell
asdf plugin add python
```

##
