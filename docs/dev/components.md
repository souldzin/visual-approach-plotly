# Working with components

## Structure

All JavaScript source lives in `src/lib`.

Every component has 2 main parts:

1. The main component's entrypoint lives in `components/$name.react.js`. This doesn't conatin business logic or behavior. It's react glue code.
   This component also declares the `propTypes`.
2. The component in `fragments/$name.react.js`, which is the main behavior, can implement
   wahtever as a good-ol-fashioned React functional component. It simply imports the `propTypes` from
   the entrypoint in `components/...`.

And every component has 2 main references:

1. An entry in `index.js` which imports the main entrypoint component in `components/...`
2. An entry in `LazyLoader.js` which exports **where** the main behavior component in `fragmenets/...`

## Dev workflow - Full

There is a Backend and Frontend portion to the components. You can run a full build with:

```shell
npm run build
```

And start a python dev server with:

```shell
python usage.py
```

**WARNING!** There is no live reloading here. You'll need to re run `npm run build` if you
have changed the Frontend files. You will need to **stop** the python server if you
have changes the Backend files.

## Dev workflow - Frontend Only

If you are just focusing on the Frontend part of the component, you can conveniently use
`webpack-serve`:

```shell
npm run start

# If you run into port failures you can specify a port with. The first `--` tells npm to pass to the `script`
npm run start -- --port 55556
```

The entrypoint here is in `src/demo/index.js` and the top-level `index.html`.

The `src/demo` directory holds demo code just for testing the components through `webpack-serve`
